<?php
declare(strict_types=1);

if (!function_exists('hidden_real_name')) {
    /**
     * 隐藏真实姓名
     * @param string $str
     * @return string
     * @author yinxu
     * @date 2024/3/29 20:04:59
     */
    function hidden_real_name(string $str): string
    {
        $length = mb_strlen($str, 'utf-8');
        if ($length == 2) return mb_substr($str, 0, 1, 'utf-8') . '*';
        if ($length >= 3) return mb_substr($str, 0, 1, 'utf-8') . '*' . mb_substr($str, -1, 1, 'utf-8');
        return $str;
    }
}

if (!function_exists('hidden_mobile')) {
    /**
     * 隐藏手机号
     * @param string $str
     * @return string
     * @author yinxu
     * @date 2024/3/29 20:04:35
     */
    function hidden_mobile(string $str): string
    {
        if (empty($str)) return $str;
        return substr_replace($str, '****', 3, 4);
    }
}

if (!function_exists('hidden_id_card')) {
    /**
     * 隐藏身份证
     * @param string $str
     * @return string
     * @author yinxu
     * @date 2024/3/29 20:05:13
     */
    function hidden_id_card(string $str): string
    {
        if (empty($str)) return $str;
        return substr($str, 0, 3) . (strlen($str) == 15 ? '************' : '***************');
    }
}

if (!function_exists('hidden_bank_card_no')) {
    /**
     * 隐藏银行卡号
     * @param string $str
     * @return string
     * @author yinxu
     * @date 2024/3/29 20:05:57
     */
    function hidden_bank_card_no(string $str): string
    {
        if (empty($str)) return $str;
        return substr($str, 0, 4) . " **** **** " . substr($str, -4, 4);
    }
}

if (!function_exists('str_random')) {
    /**
     * 随机字符串
     * @param int $length
     * @return string
     * @author yinxu
     * @date 2024/3/29 19:53:00
     */
    function str_random(int $length = 16): string
    {
        $str = '';
        $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        for ($i = 0; $i < $length; $i++) $str .= substr($chars, mt_rand(0, strlen($chars) - 1), 1);
        return $str;
    }
}

if (!function_exists('num_random')) {
    /**
     * 随机数字
     * @param int $length
     * @return string
     * @author yinxu
     * @date 2024/3/29 19:57:36
     */
    function num_random(int $length = 6): string
    {
        $str = '';
        $chars = '0123456789';
        $str .= substr('123456789', mt_rand(0, 8), 1);
        for ($i = 0; $i < ($length - 1); $i++) $str .= substr($chars, mt_rand(0, strlen($chars) - 1), 1);
        return $str;
    }
}

if (!function_exists('supplement_zero')) {
    /**
     * 数字补零
     * @param int $num 数字
     * @param int $len 长度（位数不够补零）
     * @return string
     * @author yinxu
     * @date 2024/3/29 20:00:31
     */
    function supplement_zero(int $num, int $len = 2): string
    {
        $num_len = strlen("$num");
        if ($num_len >= $len) return "$num";
        for ($i = 0; $i < $len - $num_len; $i++) {
            $num = "0" . "$num";
        }
        return "$num";
    }
}

if (!function_exists('aes_encode')) {
    /**
     * Aes加密
     * @param $data
     * @param string $key
     * @param string $iv
     * @param string $method
     * @return string
     * @author yinxu
     * @date 2024/3/29 18:50:01
     */
    function aes_encode($data, string $key, string $iv = '1234567890123456', string $method = 'AES-128-CBC'): string
    {
        return base64_encode(openssl_encrypt($data, $method, $key, OPENSSL_RAW_DATA, $iv));
    }
}

if (!function_exists('aes_decode')) {
    /**
     * Aes解密
     * @param $data
     * @param string $key
     * @param string $iv
     * @param string $method
     * @return false|string
     * @author yinxu
     * @date 2024/3/29 18:51:02
     */
    function aes_decode($data, string $key, string $iv = '1234567890123456', string $method = 'AES-128-CBC')
    {
        return openssl_decrypt(base64_decode($data), $method, $key, OPENSSL_RAW_DATA, $iv);
    }
}

if (!function_exists('str_encode')) {
    /**
     * UTF8字符串加密
     * @param $string
     * @return string
     * @author yinxu
     * @date 2024/3/29 18:58:02
     */
    function str_encode($string): string
    {
        list($chars, $length) = ['', strlen($string = mb_convert_encoding($string, 'UTF-8', 'GBK'))];
        for ($i = 0; $i < $length; $i++) {
            $chars .= str_pad(base_convert((string)ord($string[$i]), 10, 36), 2, "0", 0);
        }
        return $chars;
    }
}

if (!function_exists('str_decode')) {
    /**
     * UTF8字符串解密
     * @param $string
     * @return array|false|string
     * @author yinxu
     * @date 2024/3/29 18:58:10
     */
    function str_decode($string)
    {
        $chars = '';
        foreach (str_split($string, 2) as $char) {
            $chars .= chr(intval(base_convert($char, 36, 10)));
        }
        return mb_convert_encoding($chars, 'GBK', 'UTF-8');
    }
}

if (!function_exists('unicode_encode')) {
    /**
     * 中文转字符串
     * @param string $str
     * @return string
     * @author Even <even@1000duo.cn>
     * @date 2019/08/28 下午5:29
     */
    function unicodeEncode(string $str): string
    {
        preg_match_all('/./u', $str, $matches);
        $unicodeStr = "";
        foreach ($matches[0] as $m) {
            $unicodeStr .= "&#" . base_convert(bin2hex(iconv('UTF-8', "UCS-4", $m)), 16, 10);
        }
        return $unicodeStr;
    }
}

if (!function_exists('unicode_decode')) {
    /**
     * 字符串转中文
     * @param string $unicode_str
     * @return string
     * @author Even <even@1000duo.cn>
     * @date 2019/08/28 下午5:30
     */
    function unicodeDecode(string $unicode_str): string
    {
        $json = '{"str":"' . $unicode_str . '"}';
        $arr = json_decode($json, true);
        if (empty($arr)) return '';
        return $arr['str'];
    }
}

if (!function_exists('emoji_encode')) {
    /**
     * Emoji原形转换为String
     * @param string $content
     * @return string
     */
    function emoji_encode(string $content): string
    {
        return json_decode(preg_replace_callback("/(\\\u[ed][0-9a-f]{3})/i", function ($string) {
            return addslashes($string[0]);
        }, json_encode($content)));
    }
}

if (!function_exists('emoji_decode')) {
    /**
     * Emoji字符串转换为原形
     * @param string $content
     * @return string
     */
    function emoji_decode(string $content): string
    {
        return json_decode(preg_replace_callback('/\\\\\\\\/i', function () {
            return '\\';
        }, json_encode($content)));
    }
}

if (!function_exists('bool2str')) {
    function bool2str($bool, string $default = "NO"): string
    {
        if (is_bool($bool)) return $bool ? "YES" : "NO";
        if (is_string($bool)) return $bool == "YES" ? "YES" : "NO";
        return $default;
    }
}

if (!function_exists('str2bool')) {
    function str2bool($str, bool $default = false): bool
    {
        if (is_bool($str)) return $str == true;
        if (is_string($str)) return $str == "YES";
        return $default;
    }
}

if (!function_exists('json2arr')) {
    /**
     * Json转Array
     * @param string $json
     * @return mixed
     * @throws Exception
     * @author yinxu
     * @date 2024/3/29 18:52:32
     */
    function json2arr(string $json)
    {
        $result = json_decode($json, true);
        if (empty($result)) {
            throw new \Exception('invalid json str.', 0);
        }
        return $result;
    }
}

if (!function_exists('arr2json')) {
    /**
     * Array 转 json
     * @param array $data
     * @return array|string|string[]|null
     * @author yinxu
     * @date 2024/3/29 18:53:48
     */
    function arr2json(array $data)
    {
        return preg_replace_callback('/\\\\u([0-9a-f]{4})/i', function ($matches) {
            return mb_convert_encoding(pack("H*", $matches[1]), "UTF-8", "UCS-2BE");
        }, json_encode($data));
    }
}

if (!function_exists('arr2tree')) {
    /**
     * 数组转Tree数组
     * @param array $list 原数据
     * @param string $idField id字段名
     * @param string $pidField pid字段名
     * @param int|string $pid pid根值
     * @param array $treeList 最终数组
     * @param string $childrenField 子节点字段名
     * @param int $maxLevel 最大级数
     * @param int $level 当前级数
     * @author yinxu
     * @date 2024/3/29 19:02:43
     */
    function arr2tree(array $list, string $idField, string $pidField, $pid = 0, array &$treeList = [], string $childrenField = 'children', int $maxLevel = 0, int $level = 1)
    {
        foreach ($list as $key => $item) {
            if ($item[$pidField] === $pid) {
                $temp = $item;
                unset($list[$key]);
                $temp[$childrenField] = [];
                if ($maxLevel < 1 || $level < $maxLevel) arr2tree($list, $idField, $pidField, $item[$idField], $temp[$childrenField], $childrenField, $maxLevel, $level + 1);
                if (empty($temp[$childrenField])) unset($temp[$childrenField]);
                $treeList[] = $temp;
                unset($temp);
            }
        }
    }
}

if (!function_exists('excel_read')) {
    /**
     * 读取Excel文件
     * @param $filename
     * @return array
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     * @author yinxu
     * @date 2024/3/29 19:43:19
     */
    function read($filename): array
    {
        if (!is_file($filename)) throw new \InvalidArgumentException('文件不存在');
        $extension = strtolower(pathinfo($filename, PATHINFO_EXTENSION));
        switch ($extension) {
            case 'xlsx':
                $objReader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader('Xlsx');
                break;
            case 'xls':
                $objReader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader('Xls');
                break;
            default:
                throw new \InvalidArgumentException('文件格式不正确，只支持xls,xlsx格式！');
        }
        $objReader->setReadDataOnly(true);
        $objPHPExcel = $objReader->load($filename);
        $objWorksheet = $objPHPExcel->getActiveSheet();
        $highestRow = $objWorksheet->getHighestRow();
        $highestColumn = $objWorksheet->getHighestColumn();
        $highestColumnIndex = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::columnIndexFromString($highestColumn);
        $excelData = [];
        for ($row = 1; $row <= $highestRow; $row++) {
            # 判断整行为空 = 所有列都为空
            $isEmpty = true;
            $rowData = [];
            for ($col = 1; $col <= $highestColumnIndex; $col++) {
                $colValue = (string)$objWorksheet->getCell(\PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($col) . $row)->getValue();
                if (!empty($colValue)) $isEmpty = false;
                $rowData[] = $colValue;
            }
            if (!$isEmpty) {
                $excelData[$row] = $rowData;
            }
        }
        return $excelData;
    }
}

if (!function_exists('excel_date')) {
    /**
     * Excel表日期格式读取
     * @param $excel_date
     * @param string $format
     * @return string
     * @author yinxu
     * @date 2024/3/29 20:02:37
     */
    function excel_date($excel_date, string $format = 'Y-m-d'): string
    {
        if (empty($excel_date)) {
            return "";
        } else if (is_numeric($excel_date)) {
            return date($format, ($excel_date - 25569) * 86400 - 28800);
        } elseif (preg_match('/^\d{4}-\d{2}-\d{2}$/', $excel_date)) {
            return date($format, strtotime($excel_date));
        } elseif (preg_match('/^\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}$/', $excel_date)) {
            return date($format, strtotime($excel_date));
        } else {
            return "";
        }
    }
}

if (!function_exists('excel_export')) {
    /**
     * 导出Excel表
     * @param string $title
     * @param string $name
     * @param array $columns
     * @param array $data
     * @param string $creator
     * @param string $mergeKey
     * @param Closure|null $closure
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     * @author yinxu
     * @date 2024/3/29 19:35:00
     */
    function excel_export(string $title = '', string $name = '', array $columns = [], array $data = [], string $creator = 'system', string $mergeKey = '', \Closure $closure = null)
    {

        $objPHPExcel = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
        # 创建人
        $objPHPExcel->getProperties()->setCreator($creator);
        $objPHPExcel->getProperties()->setTitle($title);
        # 当前表sheet
        $objPHPExcel->setActiveSheetIndex(0);
        # 设置当前表sheet0的标题
        $objPHPExcel->getActiveSheet()->setTitle(date('Y年m月d日'));
        # 设置居中
        $CellsName = [
            'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
            'AA', 'AB', 'AC', 'AD', 'AE', 'AF', 'AG', 'AH', 'AI', 'AJ', 'AK', 'AL', 'AM', 'AN', 'AO', 'AP', 'AQ', 'AR', 'AS', 'AT', 'AU', 'AV', 'AW', 'AX', 'AY', 'AZ',
            'BA', 'BB', 'BC', 'BD', 'BE', 'BF', 'BG', 'BH', 'BI', 'BJ', 'BK', 'BL', 'BM', 'BN', 'BO', 'BP', 'BQ', 'BR', 'BS', 'BT', 'BU', 'BV', 'BW', 'BX', 'BY', 'BZ',
        ];
        foreach ($columns as $key => $item) {
            $objPHPExcel->getActiveSheet()->setCellValue("{$CellsName[$key]}1", $item['title'] ?? $item['comment']);
            $objPHPExcel->getActiveSheet()->getColumnDimension("{$CellsName[$key]}")->setWidth($item['width'] ?? 18);
        }

        if (!!$closure) {
            # 闭包回调
            $objPHPExcel = $closure($objPHPExcel, $CellsName);
        } else {
            # 设置当前表sheet0的内容
            $hasMerge = !empty($mergeKey);
            $currentKey = "";
            $currentStart = 0;
            $currentEnd = 0;
            foreach ($data as $k => $v) {
                $num = $k + 2;
                if ($hasMerge && !isset($v[$mergeKey])) $hasMerge = false;
                if ($hasMerge) {
                    # 第一条数据
                    if (empty($currentKey)) {
                        $currentKey = $v[$mergeKey];
                        $currentStart = $num;
                        $currentEnd = $num;
                    } else if ($currentKey == $v[$mergeKey]) {
                        $currentEnd = $num;
                    } else if ($currentKey != $v[$mergeKey]) {
                        # 值相等就不处理,处理合并
                        if ($currentEnd > $currentStart) {
                            foreach ($columns as $kk => $vv) {
                                # 开始行号：$currentStart;
                                # 结算行号：$currentEnd;
                                # 先合并单元格
                                # 修改底部边框颜色
                                $objPHPExcel->getActiveSheet()->getStyle("{$CellsName[$kk]}" . $currentEnd)
                                    ->getBorders()->getBottom()->setBorderStyle("thin")->getColor()->setARGB('00a1c7fe');
                                if (!isset($vv['merge']) || !$vv['merge']) continue;
                                $objPHPExcel->setActiveSheetIndex(0)
                                    ->mergeCells("{$CellsName[$kk]}" . $currentStart . ":" . "{$CellsName[$kk]}" . $currentEnd);
                            }
                        } else {
                            foreach ($columns as $kk => $vv) {
                                $objPHPExcel->getActiveSheet()->getStyle("{$CellsName[$kk]}" . $currentEnd)
                                    ->getBorders()->getBottom()->setBorderStyle("thin")->getColor()->setARGB('00a1c7fe');
                            }
                        }
                        # 合并完毕
                        $currentKey = $v[$mergeKey];
                        $currentStart = $num;
                        $currentEnd = $num;
                    }
                }
                # 获取
                foreach ($columns as $kk => $vv) {
                    // 列数据
                    if ($vv['name'] === '#') {
                        $objPHPExcel->setActiveSheetIndex(0)->setCellValue("{$CellsName[$kk]}" . $num, ($k + 1) . " ");
                    } else {
                        # 数据类型
                        $value = $v[$vv['name']] ?? '';
                        if (!empty($vv['options']) && !empty($vv['type']) && in_array($vv['type'], ['status', 'select'])) {
                            foreach ($vv['options'] as $op) {
                                if ($op['value'] == $value) $value = $op['label'];
                            }
                        }
                        # 写入数据表
                        $objPHPExcel->setActiveSheetIndex(0)->setCellValueExplicit("{$CellsName[$kk]}" . $num, $value, 's');
                        if (isset($vv['callback']) && is_callable($vv['callback'])) {
                            $callback = $vv['callback'];
                            $callback($v, $k, $data, $objPHPExcel, $CellsName[$kk], $num);
                        }
                    }
                    # 自动换行,加垂直居中
                    $objPHPExcel->getActiveSheet()->getStyle("{$CellsName[$kk]}" . $num)
                        ->getAlignment()->setVertical("center")->setWrapText(($vv['wrap'] ?? false) == true);
//                    $objPHPExcel->getActiveSheet()->getRowDimension($num)->setRowHeight(20);
                }
            }
            if ($hasMerge && $currentEnd > $currentStart) {
                foreach ($columns as $kk => $vv) {
                    # 开始行号：$currentStart;
                    # 结算行号：$currentEnd;
                    # 先合并单元格
                    if (!isset($vv['merge']) || !$vv['merge']) continue;
                    $objPHPExcel->setActiveSheetIndex(0)
                        ->mergeCells("{$CellsName[$kk]}" . $currentStart . ":" . "{$CellsName[$kk]}" . $currentEnd);
                }
            }
        }
        $activeSheet = $objPHPExcel->getActiveSheet(); // 获取当前活动的工作表
        $rowHeight = 18;
        for ($row = 1; $row <= $activeSheet->getHighestRow(); $row++) {
            $activeSheet->getRowDimension($row)->setRowHeight($rowHeight);
        }

        $name = $name . '-' . date('Ymd') . '-' . date('His');

        # 下载Excel
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control:must-revalidate, post-check=0, pre-check=0");
        header("Content-Type:application/force-download");
        header("Content-Type:application/vnd.ms-execl");
        header("Content-Type:application/octet-stream");
        header("Content-Type:application/download");

        $encoded_filename = urlencode($name);
        $ua = $_SERVER["HTTP_USER_AGENT"];
        if (preg_match("/MSIE/", $ua)) {
            header('Content-Disposition: attachment; filename="' . $encoded_filename . '.xls"');
        } else if (preg_match("/Firefox/", $ua)) {
            header('Content-Disposition: attachment; filename*="utf8\'\'' . $name . '.xls"');
        } else {
            header('Content-Disposition: attachment; filename="' . $name . '.xls"');
        }

        $objWriter = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($objPHPExcel, 'Xls');;
        ob_end_clean();
        $objWriter->save('php://output');
    }
}

if (!function_exists('excel_save')) {
    /**
     * 导出Excel表
     * @param string $title
     * @param string $name
     * @param array $columns
     * @param array $data
     * @param string $creator
     * @param string $mergeKey
     * @param Closure|null $closure
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     * @author yinxu
     * @date 2024/3/29 19:35:00
     */
    function excel_save(string $path, string $name, array $columns = [], array $data = [], string $creator = 'system', string $mergeKey = '', \Closure $closure = null)
    {

        $objPHPExcel = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
        # 创建人
        $objPHPExcel->getProperties()->setCreator($creator);
        # 当前表sheet
        $objPHPExcel->setActiveSheetIndex(0);
        # 设置当前表sheet0的标题
        $objPHPExcel->getActiveSheet()->setTitle(date('Y年m月d日'));
        # 设置居中
        $CellsName = [
            'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
            'AA', 'AB', 'AC', 'AD', 'AE', 'AF', 'AG', 'AH', 'AI', 'AJ', 'AK', 'AL', 'AM', 'AN', 'AO', 'AP', 'AQ', 'AR', 'AS', 'AT', 'AU', 'AV', 'AW', 'AX', 'AY', 'AZ',
            'BA', 'BB', 'BC', 'BD', 'BE', 'BF', 'BG', 'BH', 'BI', 'BJ', 'BK', 'BL', 'BM', 'BN', 'BO', 'BP', 'BQ', 'BR', 'BS', 'BT', 'BU', 'BV', 'BW', 'BX', 'BY', 'BZ',
        ];
        foreach ($columns as $key => $item) {
            $objPHPExcel->getActiveSheet()->setCellValue("{$CellsName[$key]}1", $item['title'] ?? $item['comment']);
            $objPHPExcel->getActiveSheet()->getColumnDimension("{$CellsName[$key]}")->setWidth($item['width'] ?? 18);
        }

        if (!!$closure) {
            # 闭包回调
            $objPHPExcel = $closure($objPHPExcel, $CellsName);
        } else {
            # 设置当前表sheet0的内容
            $hasMerge = !empty($mergeKey);
            $currentKey = "";
            $currentStart = 0;
            $currentEnd = 0;
            foreach ($data as $k => $v) {
                $num = $k + 2;
                if ($hasMerge && !isset($v[$mergeKey])) $hasMerge = false;
                if ($hasMerge) {
                    # 第一条数据
                    if (empty($currentKey)) {
                        $currentKey = $v[$mergeKey];
                        $currentStart = $num;
                        $currentEnd = $num;
                    } else if ($currentKey == $v[$mergeKey]) {
                        $currentEnd = $num;
                    } else if ($currentKey != $v[$mergeKey]) {
                        # 值相等就不处理,处理合并
                        if ($currentEnd > $currentStart) {
                            foreach ($columns as $kk => $vv) {
                                # 开始行号：$currentStart;
                                # 结算行号：$currentEnd;
                                # 先合并单元格
                                # 修改底部边框颜色
                                $objPHPExcel->getActiveSheet()->getStyle("{$CellsName[$kk]}" . $currentEnd)
                                    ->getBorders()->getBottom()->setBorderStyle("thin")->getColor()->setARGB('00a1c7fe');
                                if (!isset($vv['merge']) || !$vv['merge']) continue;
                                $objPHPExcel->setActiveSheetIndex(0)
                                    ->mergeCells("{$CellsName[$kk]}" . $currentStart . ":" . "{$CellsName[$kk]}" . $currentEnd);
                            }
                        } else {
                            foreach ($columns as $kk => $vv) {
                                $objPHPExcel->getActiveSheet()->getStyle("{$CellsName[$kk]}" . $currentEnd)
                                    ->getBorders()->getBottom()->setBorderStyle("thin")->getColor()->setARGB('00a1c7fe');
                            }
                        }
                        # 合并完毕
                        $currentKey = $v[$mergeKey];
                        $currentStart = $num;
                        $currentEnd = $num;
                    }
                }
                # 获取
                foreach ($columns as $kk => $vv) {
                    // 列数据
                    if ($vv['name'] === '#') {
                        $objPHPExcel->setActiveSheetIndex(0)->setCellValue("{$CellsName[$kk]}" . $num, ($k + 1) . " ");
                    } else {
                        # 数据类型
                        $value = $v[$vv['name']] ?? '';
                        if (!empty($vv['options']) && !empty($vv['type']) && in_array($vv['type'], ['status', 'select'])) {
                            foreach ($vv['options'] as $op) {
                                if ($op['value'] == $value) $value = $op['label'];
                            }
                        }
                        # 写入数据表
                        $objPHPExcel->setActiveSheetIndex(0)->setCellValueExplicit("{$CellsName[$kk]}" . $num, $value, 's');
                        if (isset($vv['callback']) && is_callable($vv['callback'])) {
                            $callback = $vv['callback'];
                            $callback($v, $k, $data, $objPHPExcel, $CellsName[$kk], $num);
                        }
                    }
                    # 自动换行,加垂直居中
                    $objPHPExcel->getActiveSheet()->getStyle("{$CellsName[$kk]}" . $num)
                        ->getAlignment()->setVertical("center")->setWrapText(($vv['wrap'] ?? false) == true);
//                    $objPHPExcel->getActiveSheet()->getRowDimension($num)->setRowHeight(20);
                }
            }
            if ($hasMerge && $currentEnd > $currentStart) {
                foreach ($columns as $kk => $vv) {
                    # 开始行号：$currentStart;
                    # 结算行号：$currentEnd;
                    # 先合并单元格
                    if (!isset($vv['merge']) || !$vv['merge']) continue;
                    $objPHPExcel->setActiveSheetIndex(0)
                        ->mergeCells("{$CellsName[$kk]}" . $currentStart . ":" . "{$CellsName[$kk]}" . $currentEnd);
                }
            }
        }
        $activeSheet = $objPHPExcel->getActiveSheet(); // 获取当前活动的工作表
        $rowHeight = 18;
        for ($row = 1; $row <= $activeSheet->getHighestRow(); $row++) {
            $activeSheet->getRowDimension($row)->setRowHeight($rowHeight);
        }

        $date = date('Ymd');
        $filename = md5($name . '-' . microtime(true)).'.xls';
        $path = str_ends_with('/', $path) ? $path : $path . '/';
        $path = $path . $date;

        if (!is_dir($path)) {
            @mkdir($path);
        }

        $objWriter = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($objPHPExcel, 'Xls');;
        $objWriter->save($path . '/' . $filename);

        return $date . '/' . $filename;
    }
}
